# Payments Gateway

Payments Gateway purpose is to provide a backend service for CRUD operations on payments objects. Example client of this
project is a front-end application written in JS or native mobile application. Gateway exposes a RESTful API capable of
creating, updating, deleting, fetching and listing payments. Created payments are persisted in a database. Updating
is handled by replacing whole object.

## Features

- Fetching single payment.
- Listing all not deleted payments.
- Creating a new payment with validation.
- Updating (replacing) given payment with validation.
- Deleting (marking as deleted) payments.
- Content-Type validation on POST and PUT methods.
- Location header returned when creating payments with POST.
- Error messages encoded as JSON.
- In-memory database for testing.
- SQLite database for development.
- Dockerfile for building and running.

## Known Limitations

There are a few missing features that should be considered in bigger scope:

- Partial updates via PATCH method with field masks.
- Filtering of payments when listing.
- Gzip support based on Accept header.
- ETAG calculated for objects to allow client-side caching.

## Running

Sample SQLite DB is provided in this repository (`sample.db`) which is used by default when running gateway without
any flags:

`go run main.go`

By default it will listen on `:8080` but it can be changed with flag. See `go run main.go -h` for usage.

To use your own SQLite DB, first create it with:

`sqlite3 payments.db`

Next, paste contents of `db/schema.sql` into `sqlite3` terminal to create the DB schema. Now payments gateway can be
started locally with:

`go run main.go -db payments.db`

## Dep

This projects is using Dep (https://github.com/golang/dep) for dependencies. All deps are vendored and checked in the
repository under `vendor/`.

## Testing

Payments service is tested using standard library `testing` and `httptest` packages to record and launch requests.
Table tests approach is used for clean tests code. TDD approach was used during development:

- Create empty handler that returns nil.
- Create in-memory store.
- Write tests that must fail.
- Implement handler code until tests pass.
- Refactor handler / in-memory store.

Run tests with:

`go test ./...`

## Docker

Payments Gateway can be built and run inside a Docker container. Multi-stage docker building process is provided in
`Dockerfile`. To build it, run:

`docker build -t payments-gateway:0.1 .`

It will contain and use `sample.db` SQLite database by default. To run it (in foreground):

`docker run -p31337:8080 -it --rm --name payments-gateway payments-gateway:0.1`

Replace `31337` with your local port of choice. Payments Gateway API will can be accessed at http://localhost:31337/v0.
