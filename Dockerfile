FROM golang:1.10.3-alpine3.7
RUN apk add --no-cache sqlite-libs sqlite-dev build-base

COPY . /go/src/gitlab.com/pawelkowalak/payments-gateway
RUN go install gitlab.com/pawelkowalak/payments-gateway

FROM alpine:3.8

COPY --from=0 /go/bin/payments-gateway /payments-gateway
COPY sample.db /
CMD ["/payments-gateway", "-db", "sample.db"]
