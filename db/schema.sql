CREATE TABLE payments (
  id TEXT,
  type TEXT,
  version INT,
  organisation_id TEXT,
  attributes JSON,
  created_at TEXT,
  updated_at TEXT,
  deleted_at TEXT
)