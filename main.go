package main

import (
	"flag"
	"net/http"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"go.uber.org/zap"

	"gitlab.com/pawelkowalak/payments-gateway/payments"
)

var (
	addr   = flag.String("addr", ":8080", "Address to listen on.")
	dbPath = flag.String("db", "sample.db", "SQLite DB path.")
)

func main() {
	flag.Parse()
	// Initialise ZAP logger.
	logger, err := zap.NewDevelopment()
	if err != nil {
		panic(err)
	}
	defer logger.Sync()
	zap.ReplaceGlobals(logger)
	log := logger.Sugar()

	// Prepare DB connection.
	db, err := sqlx.Connect("sqlite3", *dbPath)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	log.Infof("Starting HTTP listener on '%s'", *addr)
	psrv := payments.NewService(payments.NewSQLiteStore(db))
	log.Fatal(http.ListenAndServe(*addr, psrv.Router()))
}
