package payments

import (
	"sync"
	"time"
)

type memoryStore struct {
	mu sync.Mutex
	db map[string]Payment
}

// NewMemoryStore creates a new in-memory store based on map and protected by mutex. Use it for testing only.
func NewMemoryStore() *memoryStore {
	return &memoryStore{
		db: make(map[string]Payment),
	}
}

func (m *memoryStore) Get(id string) (*Payment, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	p, ok := m.db[id]
	if !ok || p.DeletedAt != nil && *p.DeletedAt != "" {
		return nil, ErrNotFound
	}
	return &p, nil
}

func (m *memoryStore) List() ([]*Payment, error) {
	payments := make([]*Payment, 0, len(m.db))
	m.mu.Lock()
	for _, v := range m.db {
		if v.DeletedAt != nil && *v.DeletedAt != "" {
			continue
		}
		p := v
		payments = append(payments, &p)
	}
	m.mu.Unlock()
	return payments, nil
}

func (m *memoryStore) Create(p *Payment) error {
	m.mu.Lock()
	m.db[p.ID] = *p
	m.mu.Unlock()
	return nil
}

func (m *memoryStore) Update(id string, updates *Payment) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	p, ok := m.db[id]
	if !ok || p.DeletedAt != nil && *p.DeletedAt != "" {
		return ErrNotFound
	}
	m.db[id] = *updates
	return nil
}

func (m *memoryStore) Delete(id string) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	p, ok := m.db[id]
	if !ok || p.DeletedAt != nil && *p.DeletedAt != "" {
		return ErrNotFound
	}
	now := time.Now().Format(time.RFC3339Nano)
	p.DeletedAt = &now
	m.db[id] = p
	return nil
}
