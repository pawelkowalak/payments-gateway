package payments

import (
	"encoding/json"
	"fmt"
	"net/http"

	"go.uber.org/zap"
)

// Custom HTTP handler that can return errors. All handlers are casted to this type which has ServeHTTP method defined
// to satisfy stdlib http handler.
type jsonError func(http.ResponseWriter, *http.Request) error

const jsonContentType = "application/json"

// ServeHTTP calls original handler and handles returned error. If custom
// handlerError is thrown, use its message and HTTP status code. Otherwise
// log original error and return 503 to user without exposing real error.
func (fn jsonError) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log := zap.S()
	if err := fn(w, r); err != nil {
		w.Header().Add("Content-Type", jsonContentType)
		if herr, ok := err.(handlerError); ok {
			w.WriteHeader(herr.Code)
			fmt.Fprintln(w, herr.Error())
		} else {
			log.Info(err)
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintln(w, defaultInternalServerError)
		}
	}
}

const defaultInternalServerError = `{"code": 503,"message":"Internal server error"}`

// Custom error type for our handlers.
type handlerError struct {
	Code        int            `json:"code,omitempty"`
	Message     string         `json:"message"`
	Description string         `json:"description,omitempty"`
	Errors      []handlerError `json:"errors,omitempty"`
}

// Error satisfies error interface.
func (h handlerError) Error() string {
	log := zap.S()
	resp, err := json.Marshal(h)
	if err != nil {
		log.Infof("Cannot encode error message %v: %v\n", h, err)
		return defaultInternalServerError
	}
	return string(resp)
}

// Middleware function to check if proper Content-Type is set in the request.
func jsonRequired(next jsonError) jsonError {
	return func(w http.ResponseWriter, r *http.Request) error {
		if r.Header.Get("Content-Type") != jsonContentType {
			return handlerError{Code: http.StatusUnsupportedMediaType, Message: "Content-Type not supported"}
		}
		return next(w, r)
	}
}
