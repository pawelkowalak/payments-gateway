package payments

const (
	queryGet = `
SELECT * FROM payments
WHERE
	id = :id
AND
	deleted_at IS NULL`

	queryList = `
SELECT * FROM payments
WHERE
	deleted_at IS NULL`

	queryCreate = `
INSERT INTO payments
	(id, type, version, organisation_id, attributes, created_at)
VALUES
	(:id, :type, :version, :organisation_id, :attributes, :created_at)`

	queryUpdate = `
UPDATE payments
SET
	(type, version, organisation_id, attributes, updated_at) = (:type, :version, :organisation_id, :attributes, :updated_at)
WHERE
	id = :id
AND
	deleted_at IS NULL`

	queryDelete = `
UPDATE payments
SET
	deleted_at = $1
WHERE
	id = $2
AND
	deleted_at IS NULL`
)
