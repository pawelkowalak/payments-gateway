package payments

import (
	"errors"
)

// Store describes any store implementation behaviour (SQL, noSQL, in-memory) that can be plugged into the payments
// service.
type Store interface {
	// Get fetches single payment from store.
	Get(id string) (*Payment, error)
	// List fetches a slice of payments from store (excluding deleted ones).
	List() ([]*Payment, error)
	// Create creates a new payment in store (make sure to provide unique ID).
	Create(payment *Payment) error
	// Update updates whole payment object with new one for given ID.
	Update(id string, payment *Payment) error
	// Delete deletes (or marks as deleted) payment for given ID.
	Delete(id string) error
}

// ErrNotFound should be returned by store implementation when trying to fetch, update or delete non-existing payment.
var ErrNotFound = errors.New("payment not found")
