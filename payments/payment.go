package payments

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"

	"github.com/shopspring/decimal"
)

// Payment represents root object of payments gateway.
type Payment struct {
	Type           Type        `json:"type,omitempty"`
	ID             string      `json:"id,omitempty"`
	Version        int         `json:"version"`
	OrganisationID string      `json:"organisation_id,omitempty" db:"organisation_id"`
	Attributes     *Attributes `json:"attributes,omitempty"`
	CreatedAt      *string     `json:"-" db:"created_at"`
	UpdatedAt      *string     `json:"-" db:"updated_at"`
	DeletedAt      *string     `json:"-" db:"deleted_at"`
}

// Type is a Payment type.
type Type string

// Currently only Payment type is being used.
const (
	TypePayment Type = "Payment"
)

// Attributes holds detailed information about Payment.
type Attributes struct {
	Amount               decimal.Decimal     `json:"amount,omitempty"`
	BeneficiaryParty     *Party              `json:"beneficiary_party,omitempty"`
	ChargesInformation   *ChargesInformation `json:"charges_information,omitempty"`
	Currency             string              `json:"currency,omitempty"`
	DebtorParty          *Party              `json:"debtor_party,omitempty"`
	EndToEndReference    string              `json:"end_to_end_reference,omitempty"`
	Fx                   *Fx                 `json:"fx,omitempty"`
	NumericReference     string              `json:"numeric_reference,omitempty"`
	PaymentID            string              `json:"payment_id,omitempty"`
	PaymentPurpose       string              `json:"payment_purpose,omitempty"`
	PaymentScheme        string              `json:"payment_scheme,omitempty"`
	PaymentType          string              `json:"payment_type,omitempty"`
	ProcessingDate       string              `json:"processing_date,omitempty"`
	Reference            string              `json:"reference,omitempty"`
	SchemePaymentSubType *SchemeType         `json:"scheme_payment_sub_type,omitempty"`
	SchemePaymentType    *SchemeType         `json:"scheme_payment_type,omitempty"`
	SponsorParty         *Party              `json:"sponsor_party,omitempty"`
}

// Value is used to store JSON object in SQLite. FIXME: Should be moved to store_sqlite_conv.go and separate types for
// DB representation should be created.
func (a Attributes) Value() (driver.Value, error) {
	return json.Marshal(a)
}

// Scan is used to fetch JSON object from SQLite. FIXME: Should be moved to store_sqlite_conv.go and separate types for
// DB representation should be created.
func (a *Attributes) Scan(i interface{}) error {
	if i == nil {
		return nil
	}
	switch t := i.(type) {
	case []byte:
		return json.Unmarshal(t, a)
	case string:
		return json.Unmarshal([]byte(t), a)
	default:
		return fmt.Errorf("can't unmarshal json from type %T", i)
	}
}

// Party represents beneficiary, debtor or sponsor party.
type Party struct {
	AccountName       string       `json:"account_name,omitempty"`
	AccountNumber     string       `json:"account_number,omitempty"`
	AccountNumberCode string       `json:"account_number_code,omitempty"`
	AccountType       *AccountType `json:"account_type,omitempty"`
	Address           string       `json:"address,omitempty"`
	BankID            string       `json:"bank_id,omitempty"`
	BankIDCode        string       `json:"bank_id_code,omitempty"`
	Name              string       `json:"name,omitempty"`
}

// AccountType is an account type of Party.
type AccountType int

// Currently only Zero-type is being used.
const (
	AccountTypeZero AccountType = iota
)

// ChargesInformation holds info about charges for payment.
type ChargesInformation struct {
	BearerCode              string          `json:"bearer_code,omitempty"`
	SenderCharges           []*Charge       `json:"sender_charges,omitempty"`
	ReceiverChargesAmount   decimal.Decimal `json:"receiver_charges_amount,omitempty"`
	ReceiverChargesCurrency string          `json:"receiver_charges_currency,omitempty"`
}

// Charge holds info about charge amount and currency for payment.
type Charge struct {
	Amount   decimal.Decimal `json:"amount,omitempty"`
	Currency string          `json:"currency,omitempty"`
}

// Fx holds info about exchange rate for payment.
type Fx struct {
	ContractReference string          `json:"contract_reference,omitempty"`
	ExchangeRate      string          `json:"exchange_rate,omitempty"`
	OriginalAmount    decimal.Decimal `json:"original_amount,omitempty"`
	OriginalCurrency  string          `json:"original_currency,omitempty"`
}

// SchemeType is a scheme type or sub-type of payment.
type SchemeType string

const (
	SchemeTypeImmediatePayment SchemeType = "ImmediatePayment"
	SchemeTypeInternetBanking  SchemeType = "InternetBanking"
)
