package payments

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
)

type service struct {
	store Store
}

// NewService returns a new payments service with given store implementation.
func NewService(store Store) *service {
	return &service{store: store}
}

// Router returns a new gorilla/mux router configured for specific paths (all of them start with current API version v0).
func (s *service) Router() *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	v0 := r.PathPrefix("/v0").Subrouter()
	v0.Handle("/payments/{id}", jsonError(s.Get)).Methods("GET")
	v0.Handle("/payments", jsonError(s.List)).Methods("GET")
	v0.Handle("/payments", jsonError(jsonRequired(s.Create))).Methods("POST")
	v0.Handle("/payments/{id}", jsonError(jsonRequired(s.Replace))).Methods("PUT")
	v0.Handle("/payments/{id}", jsonError(s.Delete)).Methods("DELETE")
	return r
}

const defaultVersion = 0

// Get fetches single payment from store or returns 404.
func (s *service) Get(w http.ResponseWriter, r *http.Request) error {
	vars := mux.Vars(r)
	p, err := s.store.Get(vars["id"])
	if err != nil {
		if errors.Cause(err) == ErrNotFound {
			return handlerError{Code: http.StatusNotFound, Message: "Payment not found"}
		}
		return err
	}

	enc := json.NewEncoder(w)
	if err := enc.Encode(p); err != nil {
		return err
	}
	return nil
}

// List fetches all non-deleted payments from store or returns an empty list.
func (s *service) List(w http.ResponseWriter, r *http.Request) error {
	payments, err := s.store.List()
	if err != nil {
		return err
	}

	w.Header().Add("Content-Type", jsonContentType)
	w.WriteHeader(http.StatusOK)
	enc := json.NewEncoder(w)
	if err := enc.Encode(payments); err != nil {
		return err
	}
	return nil
}

// Create validates, assigns new UUID and saves a payment in store and returns whole payment object.
func (s *service) Create(w http.ResponseWriter, r *http.Request) error {
	dec := json.NewDecoder(r.Body)
	p := new(Payment)
	if err := dec.Decode(p); err != nil {
		return handlerError{Code: http.StatusUnprocessableEntity, Message: "Cannot parse payment", Description: err.Error()}
	}
	if err := validateCreate(p); err != nil {
		return err
	}

	p.ID = uuid.New().String()
	p.Version = defaultVersion
	now := time.Now().Format(time.RFC3339Nano)
	p.CreatedAt = &now
	if err := s.store.Create(p); err != nil {
		return err
	}

	w.Header().Add("Location", locationHeader(r, p.ID))
	w.Header().Add("Content-Type", jsonContentType)
	w.WriteHeader(http.StatusCreated)
	enc := json.NewEncoder(w)
	if err := enc.Encode(p); err != nil {
		return err
	}
	return nil
}

// Naive location header after creating a payment resource. In real world we would have ABSOLUTE_URL configured for
// each deployment and we would use it here.
func locationHeader(r *http.Request, id string) string {
	return fmt.Sprintf("http://%s%s/%s", r.Host, r.URL.String(), id)
}

// Replace validates and updates given payment ID with provided payment object. Full data replace occurs. Returns 404
// if payment does not exists or got deleted.
func (s *service) Replace(w http.ResponseWriter, r *http.Request) error {
	dec := json.NewDecoder(r.Body)
	updates := new(Payment)
	if err := dec.Decode(updates); err != nil {
		return handlerError{Code: http.StatusUnprocessableEntity, Message: "Cannot parse payment", Description: err.Error()}
	}
	if err := validateCreate(updates); err != nil {
		return err
	}

	vars := mux.Vars(r)
	id := vars["id"]
	updates.ID = id
	updates.Version = defaultVersion
	now := time.Now().Format(time.RFC3339Nano)
	updates.UpdatedAt = &now
	if err := s.store.Update(id, updates); err != nil {
		if errors.Cause(err) == ErrNotFound {
			return handlerError{Code: http.StatusNotFound, Message: "Payment not found"}
		}
		return err
	}

	w.Header().Add("Content-Type", jsonContentType)
	w.WriteHeader(http.StatusOK)
	enc := json.NewEncoder(w)
	if err := enc.Encode(updates); err != nil {
		return err
	}
	return nil
}

// Delete deletes (or marks as deleted) payment with given ID. Returns 404 if payment already deleted or doesn't exist.
func (s *service) Delete(w http.ResponseWriter, r *http.Request) error {
	vars := mux.Vars(r)
	if err := s.store.Delete(vars["id"]); err != nil {
		if errors.Cause(err) == ErrNotFound {
			return handlerError{Code: http.StatusNotFound, Message: "Payment not found"}
		}
		return err
	}
	w.WriteHeader(http.StatusNoContent)
	return nil
}

func validateCreate(p *Payment) error {
	err := handlerError{}
	if p.ID != "" {
		err.Errors = append(err.Errors, handlerError{Message: "id must be empty"})
	}
	if p.Version != defaultVersion {
		err.Errors = append(err.Errors, handlerError{Message: "version must be empty"})
	}
	if p.Type == "" {
		err.Errors = append(err.Errors, handlerError{Message: "type is required"})
	}
	if len(err.Errors) == 0 {
		return nil
	}
	err.Code = http.StatusUnprocessableEntity
	err.Message = "Validation error"
	return err
}
