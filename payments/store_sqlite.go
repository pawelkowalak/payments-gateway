package payments

import (
	"time"

	"database/sql"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type sqliteStore struct {
	db *sqlx.DB
}

// NewSQLiteStore creates a new store based on provided sqlx connection to SQLite DB.
func NewSQLiteStore(db *sqlx.DB) *sqliteStore {
	return &sqliteStore{
		db: db,
	}
}

func (s *sqliteStore) Get(id string) (*Payment, error) {
	p := new(Payment)
	if err := s.db.Get(p, queryGet, id); err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			return nil, ErrNotFound
		}
		return nil, errors.Wrapf(err, "cannot get payment '%s' from DB", id)
	}
	return p, nil
}

func (s *sqliteStore) List() ([]*Payment, error) {
	payments := make([]*Payment, 0)
	if err := s.db.Select(&payments, queryList); err != nil {
		return nil, errors.Wrap(err, "cannot select payments from DB")
	}
	return payments, nil
}

func (s *sqliteStore) Create(p *Payment) error {
	_, err := s.db.NamedExec(queryCreate, p)
	if err != nil {
		return errors.Wrapf(err, "cannot create payments '%+v' in DB", p)
	}
	return nil
}

func (s *sqliteStore) Update(id string, updates *Payment) error {
	updates.ID = id
	res, err := s.db.NamedExec(queryUpdate, updates)
	if err != nil {
		return errors.Wrapf(err, "cannot update payment '%s' with new value '%+v'", id, updates)
	}
	count, err := res.RowsAffected()
	if err != nil {
		return errors.Wrapf(err, "cannot check rows affected after updating payment '%s'", id)
	}
	if count == 0 {
		return ErrNotFound
	}
	return nil
}

func (s *sqliteStore) Delete(id string) error {
	res, err := s.db.Exec(queryDelete, time.Now().Format(time.RFC3339Nano), id)
	if err != nil {
		return errors.Wrapf(err, "cannot delete payment '%s'")
	}
	count, err := res.RowsAffected()
	if err != nil {
		return errors.Wrapf(err, "cannot check rows affected after deleting payment '%s'", id)
	}
	if count == 0 {
		return ErrNotFound
	}
	return nil
}
