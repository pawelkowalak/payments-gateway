package payments

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"sort"
	"strings"
	"testing"
)

func TestGet(t *testing.T) {
	store := NewMemoryStore()
	store.db["foo"] = Payment{
		ID: "foo",
	}
	psrv := NewService(store)
	router := psrv.Router()

	cases := []struct {
		desc      string
		path      string
		expStatus int
		expBody   string
	}{
		{
			desc:      "ok",
			path:      "/v0/payments/foo",
			expStatus: http.StatusOK,
			expBody:   `{"id":"foo","version":0}`,
		},
		{
			desc:      "not found",
			path:      "/v0/payments/bar",
			expStatus: http.StatusNotFound,
			expBody:   `{"code":404,"message":"Payment not found"}`,
		},
	}

	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			req, err := http.NewRequest("GET", tc.path, nil)
			if err != nil {
				t.Fatal(err)
			}
			rr := httptest.NewRecorder()
			router.ServeHTTP(rr, req)

			if status := rr.Code; status != tc.expStatus {
				t.Errorf("wrong status code: got %v want %v",
					status, tc.expStatus)
			}
			if strings.TrimSpace(rr.Body.String()) != strings.TrimSpace(tc.expBody) {
				t.Errorf("wrong body: got %v want %v",
					rr.Body.String(), tc.expBody)
			}
		})
	}
}

func TestList(t *testing.T) {
	store := NewMemoryStore()
	store.db["foo"] = Payment{ID: "foo"}
	store.db["bar"] = Payment{ID: "bar"}
	store.db["baz"] = Payment{ID: "baz"}
	emptyStore := NewMemoryStore()

	cases := []struct {
		desc      string
		path      string
		store     Store
		expStatus int
		expBody   string
	}{
		{
			desc:      "ok",
			path:      "/v0/payments",
			store:     store,
			expStatus: http.StatusOK,
			expBody:   `[{"id":"foo"},{"id":"bar"},{"id":"baz"}]`,
		},
		{
			desc:      "empty store",
			path:      "/v0/payments",
			store:     emptyStore,
			expStatus: http.StatusOK,
			expBody:   `[]`,
		},
	}

	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			psrv := NewService(tc.store)
			router := psrv.Router()
			req, err := http.NewRequest("GET", tc.path, nil)
			if err != nil {
				t.Fatal(err)
			}
			rr := httptest.NewRecorder()
			router.ServeHTTP(rr, req)

			if status := rr.Code; status != tc.expStatus {
				t.Errorf("wrong status code: got %v want %v",
					status, tc.expStatus)
			}
			cmpManyPayments(t, rr.Body.Bytes(), []byte(tc.expBody), true)
		})
	}
}

func TestCreate(t *testing.T) {
	store := NewMemoryStore()
	psrv := NewService(store)
	router := psrv.Router()

	cases := []struct {
		desc        string
		body        string
		contentType string
		expStatus   int
		expBody     string
	}{
		{
			desc:        "ok",
			body:        `{"type":"Payment","version":0,"organisation_id":"foobar"}`,
			contentType: "application/json",
			expStatus:   http.StatusCreated,
			expBody:     `{"type":"Payment","id":"<uuid>","version":0,"organisation_id":"foobar"}`,
		},
		{
			desc:        "wrong content-type",
			body:        `{"type":"Payment","version":0,"organisation_id":"foobar"}`,
			contentType: "application/xml",
			expStatus:   http.StatusUnsupportedMediaType,
			expBody:     `{"code":415,"message":"Content-Type not supported"}`,
		},
		{
			desc:        "id must be empty",
			body:        `{"type":"Payment","id":"foo"}`,
			contentType: "application/json",
			expStatus:   http.StatusUnprocessableEntity,
			expBody:     `{"code":422,"message":"Validation error","errors":[{"message":"id must be empty"}]}`,
		},
		{
			desc:        "version must be empty",
			body:        `{"type":"Payment","version":3}`,
			contentType: "application/json",
			expStatus:   http.StatusUnprocessableEntity,
			expBody:     `{"code":422,"message":"Validation error","errors":[{"message":"version must be empty"}]}`,
		},
		{
			desc:        "type required",
			body:        `{"organisation_id":"foo"}`,
			contentType: "application/json",
			expStatus:   http.StatusUnprocessableEntity,
			expBody:     `{"code":422,"message":"Validation error","errors":[{"message":"type is required"}]}`,
		},
	}

	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			body := bytes.NewBuffer([]byte(tc.body))
			req, err := http.NewRequest("POST", "/v0/payments", body)
			if err != nil {
				t.Fatal(err)
			}
			req.Header.Add("Content-Type", tc.contentType)
			rr := httptest.NewRecorder()
			router.ServeHTTP(rr, req)

			if status := rr.Code; status != tc.expStatus {
				t.Errorf("wrong status code: got %v want %v",
					status, tc.expStatus)
			}
			if tc.expStatus >= 400 {
				cmpErrors(t, rr.Body.Bytes(), []byte(tc.expBody))
			} else {
				cmpPaymentsBytes(t, rr.Body.Bytes(), []byte(tc.expBody), false)
			}
		})
	}
}

func TestReplace(t *testing.T) {
	store := NewMemoryStore()
	store.db["foo"] = Payment{ID: "foo"}
	psrv := NewService(store)
	router := psrv.Router()

	cases := []struct {
		desc        string
		path        string
		body        string
		contentType string
		expStatus   int
		expBody     string
	}{
		{
			desc:        "set organisation id",
			path:        "/v0/payments/foo",
			body:        `{"organisation_id":"foobar","type":"Payment"}`,
			contentType: "application/json",
			expStatus:   http.StatusOK,
			expBody:     `{"type":"Payment","id":"foo","version":0,"organisation_id":"foobar"}`,
		},
		{
			desc:        "wrong content-type",
			path:        "/v0/payments/foo",
			body:        `{"organisation_id":"foobar","type":"Payment"}`,
			contentType: "application/xml",
			expStatus:   http.StatusUnsupportedMediaType,
			expBody:     `{"code":415,"message":"Content-Type not supported"}`,
		},
		{
			desc:        "not found",
			path:        "/v0/payments/bar",
			body:        `{"organisation_id":"foobar","type":"Payment"}`,
			contentType: "application/json",
			expStatus:   http.StatusNotFound,
			expBody:     `{"code":404,"message":"Payment not found"}`,
		},
		{
			desc:        "id must be empty",
			path:        "/v0/payments/foo",
			body:        `{"type":"Payment","id":"foo"}`,
			contentType: "application/json",
			expStatus:   http.StatusUnprocessableEntity,
			expBody:     `{"code":422,"message":"Validation error","errors":[{"message":"id must be empty"}]}`,
		},
		{
			desc:        "version must be empty",
			path:        "/v0/payments/foo",
			body:        `{"type":"Payment","version":3}`,
			contentType: "application/json",
			expStatus:   http.StatusUnprocessableEntity,
			expBody:     `{"code":422,"message":"Validation error","errors":[{"message":"version must be empty"}]}`,
		},
		{
			desc:        "type required",
			path:        "/v0/payments/foo",
			body:        `{"organisation_id":"foo"}`,
			contentType: "application/json",
			expStatus:   http.StatusUnprocessableEntity,
			expBody:     `{"code":422,"message":"Validation error","errors":[{"message":"type is required"}]}`,
		},
	}

	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			body := bytes.NewBuffer([]byte(tc.body))
			req, err := http.NewRequest("PUT", tc.path, body)
			if err != nil {
				t.Fatal(err)
			}
			req.Header.Add("Content-Type", tc.contentType)
			rr := httptest.NewRecorder()
			router.ServeHTTP(rr, req)

			if status := rr.Code; status != tc.expStatus {
				t.Errorf("wrong status code: got %v want %v",
					status, tc.expStatus)
			}
			if tc.expStatus >= 400 {
				cmpErrors(t, rr.Body.Bytes(), []byte(tc.expBody))
			} else {
				cmpPaymentsBytes(t, rr.Body.Bytes(), []byte(tc.expBody), false)
			}
		})
	}
}

func TestDelete(t *testing.T) {
	store := NewMemoryStore()
	store.db["foo"] = Payment{ID: "foo"}
	psrv := NewService(store)
	router := psrv.Router()

	cases := []struct {
		desc      string
		path      string
		expStatus int
		expBody   string
	}{
		{
			desc:      "ok",
			path:      "/v0/payments/foo",
			expStatus: http.StatusNoContent,
		},
		{
			desc:      "not found",
			path:      "/v0/payments/bar",
			expStatus: http.StatusNotFound,
			expBody:   `{"code":404,"message":"Payment not found"}`,
		},
	}

	for _, tc := range cases {
		t.Run(tc.desc, func(t *testing.T) {
			req, err := http.NewRequest("DELETE", tc.path, nil)
			if err != nil {
				t.Fatal(err)
			}
			rr := httptest.NewRecorder()
			router.ServeHTTP(rr, req)

			if status := rr.Code; status != tc.expStatus {
				t.Errorf("wrong status code: got %v want %v",
					status, tc.expStatus)
			} else if status == http.StatusNoContent {
				getReq, err := http.NewRequest("GET", tc.path, nil)
				if err != nil {
					t.Fatal(err)
				}
				getRR := httptest.NewRecorder()
				router.ServeHTTP(getRR, getReq)
				if getRR.Code != http.StatusNotFound {
					t.Errorf("wrong status when fetching payment after deleting: %v, want %v", getRR.Code, http.StatusNotFound)
				}
			}
			if tc.expStatus >= 400 {
				cmpErrors(t, rr.Body.Bytes(), []byte(tc.expBody))
			}
		})
	}
}

func cmpManyPayments(t *testing.T, gotData, expData []byte, compareIDs bool) {
	var gots []Payment
	if err := json.Unmarshal(gotData, &gots); err != nil {
		t.Fatalf("cannot unmarshal payment '%s': %v", string(gotData), err)
		return
	}
	var exps []Payment
	if err := json.Unmarshal(expData, &exps); err != nil {
		t.Fatalf("cannot unmarshal payment '%s': %v", string(expData), err)
		return
	}
	sort.Slice(gots, func(i, j int) bool {
		return gots[i].ID < gots[j].ID
	})
	sort.Slice(exps, func(i, j int) bool {
		return exps[i].ID < exps[j].ID
	})
	for i := range exps {
		cmpPayments(t, gots[i], exps[i], compareIDs)
	}
}

func cmpPaymentsBytes(t *testing.T, gotData, expData []byte, compareIDs bool) {
	var got Payment
	if err := json.Unmarshal(gotData, &got); err != nil {
		t.Fatalf("cannot unmarshal payment '%s': %v", string(gotData), err)
		return
	}
	var exp Payment
	if err := json.Unmarshal(expData, &exp); err != nil {
		t.Fatalf("cannot unmarshal payment '%s': %v", string(expData), err)
		return
	}
	cmpPayments(t, got, exp, compareIDs)
}

func cmpPayments(t *testing.T, got, exp Payment, compareIDs bool) {
	if got.Type != exp.Type {
		t.Errorf("wrong payment type: '%s', expected '%s'", got.Type, exp.Type)
	}
	if got.Version != exp.Version {
		t.Errorf("wrong payment version: %d, expected %d", got.Version, exp.Version)
	}
	if got.OrganisationID != exp.OrganisationID {
		t.Errorf("wrong payment organisation id: '%s', expected '%s'", got.OrganisationID, exp.OrganisationID)
	}
	if compareIDs {
		if got.ID != exp.ID {
			t.Errorf("wrong payment id: '%s', expected '%s'", got.ID, exp.ID)
		}
	}
}

func cmpErrors(t *testing.T, gotData, expData []byte) {
	var got handlerError
	if err := json.Unmarshal(gotData, &got); err != nil {
		t.Fatalf("cannot unmarshal error '%s': %v", string(gotData), err)
	}
	var exp handlerError
	if err := json.Unmarshal(expData, &exp); err != nil {
		t.Fatalf("cannot unmarshal error %s: %v", string(expData), err)
	}
	if got.Code != exp.Code {
		t.Errorf("wrong error code: %d, expected %d", got.Code, exp.Code)
	}
	if got.Message != exp.Message {
		t.Errorf("wrong error message: '%s', expected '%s'", got.Message, exp.Message)
	}
	if got.Description != exp.Description {
		t.Errorf("wrong error description: '%s', expected '%s'", got.Description, exp.Description)
	}
	if len(got.Errors) != len(exp.Errors) {
		t.Errorf("wrong errors len: %d, expected %d", len(got.Errors), len(exp.Errors))
	}
	sort.Slice(got.Errors, func(i, j int) bool {
		return got.Errors[i].Message < got.Errors[j].Message
	})
	sort.Slice(exp.Errors, func(i, j int) bool {
		return exp.Errors[i].Message < exp.Errors[j].Message
	})
	for i := range exp.Errors {
		if got.Errors[i].Message != exp.Errors[i].Message {
			t.Errorf("wrong error at index[%d]: %s, expected %s", i, got.Errors[i].Message, exp.Errors[i].Message)
		}
	}
}
